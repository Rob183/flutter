import 'dart:math';
import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blueAccent.shade700,
          title: Text("Ask me anything"),
        ),
        body: Cube(),
        backgroundColor: Colors.blue,
      ),
    );
  }
}

class Cube extends StatefulWidget {
  const Cube({Key? key}) : super(key: key);

  @override
  State<Cube> createState() => _CubeState();
}

class _CubeState extends State<Cube> {
  var number = 1;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: FlatButton(
          onPressed: () {
            setState(() {
              number = Random().nextInt(5) + 1;
            });
          },
          child: Image.asset('images/ball$number.png'),
        ),
      ),
    );
  }
}
